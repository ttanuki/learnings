import os
import pathlib
from datetime import timezone, datetime

import gitlab
import sqlite_utils
from sqlite_utils.db import NotFoundError


root = pathlib.Path(__file__).parent.resolve()
gl = gitlab.Gitlab(
    "https://gitlab.com",
    private_token=os.environ.get("GITLAB_TOKEN"),
)


def created_changed_times(ref="master"):
    created_changed_times = {}
    project = gl.projects.get(id=27949899)
    commits = reversed(project.commits.list(ref_name=ref))
    for c in commits:
        co = project.commits.get(c.id)
        dt = datetime.fromisoformat(c.committed_date)
        changed_files = [f["new_path"] for f in co.diff()]
        for file in changed_files:
            if file not in created_changed_times:
                created_changed_times[file] = {
                    "created": dt.isoformat(),
                    "created_utc": dt.astimezone(timezone.utc).isoformat(),
                }
            created_changed_times[file].update(
                {
                    "updated": dt.isoformat(),
                    "updated_utc": dt.astimezone(timezone.utc).isoformat(),
                }
            )
    return created_changed_times


def build_database(repo_path):
    all_times = created_changed_times()
    db = sqlite_utils.Database(repo_path / "learnings.db")
    table = db.table("learnings", pk="path")
    for filepath in repo_path.glob("*/*.md"):
        fp = filepath.open()
        title = fp.readline().lstrip("#").strip()
        body = fp.read().strip()
        path = str(filepath.relative_to(root))
        slug = filepath.stem
        url = f"https://gitlab.com/ttanuki/learnings/-/tree/master/{path}"
        path_slug = path.replace("/", "_")
        try:
            row = table.get(path_slug)
            previous_body = row["body"]
            previous_html = row["html"]
        except (NotFoundError, KeyError):
            previous_body = None
            previous_html = None
        record = {
            "path": path_slug,
            "slug": slug,
            "topic": path.split("/")[0],
            "title": title,
            "url": url,
            "body": body,
        }
        if (body != previous_body) or not previous_html:
            try:
                html = gl.markdown(body)
                if html:
                    record["html"] = html
                    print(f"Rendered HTML for {path}")
            except Exception:
                assert False, f"Could not render {path}"
        if path in all_times:
            record.update(all_times[path])
        with db.conn:
            table.upsert(record, alter=True)

    table.enable_fts(
        ["title", "body"], tokenize="porter", create_triggers=True, replace=True
    )


if __name__ == "__main__":
    build_database(root)
