# My Learnings

Notes I Take to Keep Track of My Mind. Inspired _heavily_ by [simonw/til](https://github.com/simonw/til), which I found on HackerNews.

<!-- count starts -->1<!-- count ends --> Learnings since I started.

<!-- index starts -->
## arch_linux

* [Reflector as a systemd timer](https://gitlab.com/ttanuki/learnings/-/tree/master/arch_linux/Reflector_as_a_systemd_timer.md) - 2021-07-06
<!-- index ends -->
